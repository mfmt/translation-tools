# May First Translation Tools

This repo contains helper tools for translation.

## Command Line translation helpers

The `translate` command line facilitates translation between english and
spanish using the DeepL API.

It's designed to provide machine translation via key strokes in a
user-configurable way.

An example use case is for a monolingual english speaker:

 * When reading material in spanish, copy the spanish material to the clipboard
   and call `translate` via a key stroke. The translation is opened in a gedit
   temporary file for easy reading.

 * When writing material in english, copy the english material to the clipboard
   and call `translate` via a key stroke. When the material is translate, it is
   automatically placed in the clipboard so you can paste it into the document
   you are writing. 

Sample bash scripts are provide demonstrating how setting environment variables
control the behavior.

Notably: If you set a `COPY_CMD` variable, that will be used to store the
translation. On the other hand, if you provide a `READER_CMD` instead, that
command will be used to open a temporary file with the output translation.

## API Key

The May First DeepL API key is in keyringer.
